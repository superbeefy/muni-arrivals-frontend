/*global define*/

define([
    'underscore',
    'backbone',
    'models/stop'
], function (_, Backbone, StopModel) {
    'use strict';

    var RouteStopCollection = Backbone.Collection.extend({
        model: StopModel,
        route: null,
        direction: null,
        setRoute: function(route, direction) {
          this.route = route;
          this.direction = direction;
          this.fetch({reset: true});
        },
        url: function() {
          var base = '';
          if(window.location.host === 'localhost') {
            base = 'http://192.168.33.10:3000';
          }
          return base + '/route/' + this.route + '/' + this.direction;
        }
    });

    return RouteStopCollection;
});
