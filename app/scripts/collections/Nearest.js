/*global define*/

define([
    'underscore',
    'backbone',
    'models/Nearest'
  ], function (_, Backbone, NearestModel) {
    'use strict';

    var NearestCollection = Backbone.Collection.extend({
        model: NearestModel,
        latitude: null,
        longitude: null,
        setPosition: function(latitude, longitude) {
          this.latitude = latitude;
          this.longitude = longitude;
          this.fetch({reset: true});
        },
        url: function() {
          var base = '';
          if(window.location.host === 'localhost') {
            base = 'http://192.168.33.10:3000';
          }
          return base + '/nearest/' + this.latitude + '/' + this.longitude;
        }
    });

    return NearestCollection;
});
