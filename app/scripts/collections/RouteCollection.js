/*global define*/

define([
    'underscore',
    'backbone',
    'models/route'
], function (_, Backbone, RouteModel) {
    'use strict';

    var RouteCollection = Backbone.Collection.extend({
        model: RouteModel,
        url: function() {
          var base = '';
          if(window.location.host === 'localhost') {
            base = 'http://192.168.33.10:3000';
          }

          return base + '/routes';
        }
    });

    return RouteCollection;
});
