/*global define*/

define([
    'jquery',
    'underscore',
    'backbone',
    'templates',
    'models/stop'
], function ($, _, Backbone, JST, StopModel) {
    'use strict';

    var StopView = Backbone.View.extend({
        template: JST['app/scripts/templates/NearestStop.hbs'],
        className: 'stop-detailed',
        initialize: function(options) {
          this.model = new StopModel();
          this.model.setStopId(options.route, options.id);
          this.model.on('change', this.render, this);
        },
        render: function() {
          console.log("rendering stop detail");
          console.log(this.model.toJSON());
          this.$el.empty().append(this.template(this.model.toJSON()));
        }
    });

    return StopView;
});
