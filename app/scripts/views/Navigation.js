/*global define*/

define([
    'jquery',
    'underscore',
    'backbone',
    'templates'
], function ($, _, Backbone, JST) {
    'use strict';

    var NavigationView = Backbone.View.extend({
        template: JST['app/scripts/templates/Navigation.hbs'],
        initialize: function() {
          this.render();
        },
        render: function() {
          this.$el.html(this.template());
        },
        events: {
          'click .menu-button': 'toggleMenu',
          'click li.nearest': 'gotoNearest',
          'click li.routes': 'gotoRoutes'
        },
        toggleMenu: function(e) {
          var $elem = $(e.currentTarget);

          $elem.toggleClass('expand');
        },
        gotoNearest: function() {
          $('.menu-button').toggleClass('expand');
          Backbone.history.navigate('nearest', {trigger: true});
        },
        gotoRoutes: function() {
          $('.menu-button').toggleClass('expand');
          Backbone.history.navigate('routes', {trigger: true});
        }
    });

    return NavigationView;
});
