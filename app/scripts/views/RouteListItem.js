/*global define*/

define([
    'jquery',
    'underscore',
    'backbone',
    'templates'
], function ($, _, Backbone, JST) {
    'use strict';

    var RoutelistitemView = Backbone.View.extend({
        template: JST['app/scripts/templates/RouteListItem.hbs'],
        events: {
          "click li": "toggleDirections",
          "click .direction.out": "gotoOutbound",
          "click .direction.in": "gotoInbound"
        },
        render: function() {
          this.$el.html(this.template(this.model.toJSON()));

          return this;
        },
        toggleDirections: function(e) {
          var $elem = $(e.currentTarget);
          if($elem.hasClass('directions')) {
            $elem.removeClass('directions');
          } else {
            $('.routes').find('.directions').removeClass('directions');
            $elem.addClass('directions');
          }
        },
        gotoOutbound: function() {
          Backbone.history.navigate('/route/' + this.model.get('abbr') + '/outbound', true);
        },
        gotoInbound: function() {
          Backbone.history.navigate('/route/' + this.model.get('abbr') + '/inbound', true);
        }
    });

    return RoutelistitemView;
});
