/*global define*/

define([
    'jquery',
    'underscore',
    'backbone',
    'templates',
    'collections/Nearest',
    'views/NearestStop'
], function ($, _, Backbone, JST, NearestCollection, NearestStopView) {
    'use strict';

    var NearestView = Backbone.View.extend({
        template: JST['app/scripts/templates/Nearest.hbs'],
        initialize: function(options) {
          this.collection = new NearestCollection();
          this.collection.setPosition(options.latitude, options.longitude);
          this.collection.on('reset', this.render, this);
        },
        render: function() {
          this.$el.empty().append(this.template());
          _.each(this.collection.models, this.renderNearestItem, this);
        },
        renderNearestItem: function(stop) {
          var stopView = new NearestStopView({
            model: stop
          });

          this.$el.find('.detailed-stop-list').append(stopView.render().el);
        }
    });

    return NearestView;
});
