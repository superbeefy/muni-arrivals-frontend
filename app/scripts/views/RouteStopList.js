/*global define*/

define([
    'jquery',
    'underscore',
    'backbone',
    'templates',
    'collections/RouteStopCollection',
    'views/RouteStopListItem'
], function ($, _, Backbone, JST, RouteStopCollection, RouteStopListItemView) {
    'use strict';

    var RoutestoplistView = Backbone.View.extend({
        template: JST['app/scripts/templates/RouteStopList.hbs'],
        initialize: function(options) {
          this.collection = new RouteStopCollection();
          this.collection.setRoute(options.route, options.direction);
          this.collection.on('reset', this.render, this);
        },
        render: function() {
          this.$el.empty().append(this.template());
          _.each(this.collection.models, this.renderRouteStop, this);
        },

        renderRouteStop: function(stop, index) {
          stop.attributes.index = index + 1;
          var routeStopListItemView = new RouteStopListItemView({
            model: stop
          });

          this.$el.find('.stops').append(routeStopListItemView.render().el);
        }
    });

    return RoutestoplistView;
});
