/*global define*/

define([
    'jquery',
    'underscore',
    'backbone',
    'templates'
], function ($, _, Backbone, JST) {
    'use strict';

    var RouteStopListItemView = Backbone.View.extend({
        tagName: 'li',
        className: 'stop',
        template: JST['app/scripts/templates/RouteStopListItem.hbs'],
        events: {
          "click": "gotoStop"
        },
        render: function() {
          this.$el.html(this.template(this.model.toJSON()));

          return this;
        },
        gotoStop: function() {
          console.log("Navigate to stop: " + this.model.id);
          Backbone.history.navigate('stop/' + this.model.get('route') + '/'  + this.model.id, {trigger: true});
        }
    });

    return RouteStopListItemView;
});
