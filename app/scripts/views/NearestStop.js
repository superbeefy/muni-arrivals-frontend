/*global define*/

define([
    'jquery',
    'underscore',
    'backbone',
    'templates'
], function ($, _, Backbone, JST) {
    'use strict';

    var NeareststopView = Backbone.View.extend({
        template: JST['app/scripts/templates/NearestStop.hbs'],
        render: function() {
          this.$el.html(this.template(this.model.toJSON()));

          return this;
        }
    });

    return NeareststopView;
});
