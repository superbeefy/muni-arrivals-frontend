/*global define*/

define([
    'jquery',
    'underscore',
    'backbone',
    'templates',
    'collections/RouteCollection',
    'views/RouteListItem'
], function ($, _, Backbone, JST, RouteCollection, RouteListItemView) {
    'use strict';

    var RoutelistView = Backbone.View.extend({
        template: JST['app/scripts/templates/RouteList.hbs'],
        initialize: function() {
          this.collection = new RouteCollection();
          this.collection.fetch({reset: true});
          this.collection.on('reset', this.render, this);
        },

        render: function() {
          this.$el.empty().append(this.template());
          _.each(this.collection.models, this.renderRouteItem, this);
        },

        renderRouteItem: function(route) {
          var routeListItemView = new RouteListItemView({
            model: route
          });

          this.$el.find('.routes').append(routeListItemView.render().el);
        }
    });

    return RoutelistView;
});
