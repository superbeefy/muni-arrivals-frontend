/*global define*/

define([
    'underscore',
    'backbone'
], function (_, Backbone) {
    'use strict';

    var RouteModel = Backbone.Model.extend({
    });

    return RouteModel;
});
