/*global define*/

define([
    'underscore',
    'backbone'
], function (_, Backbone) {
    'use strict';

    var NearestModel = Backbone.Model.extend({
        defaults: {
        }
    });

    return NearestModel;
});
