/*global define*/

define([
    'underscore',
    'backbone'
], function (_, Backbone) {
    'use strict';

    var StopModel = Backbone.Model.extend({
      setStopId: function(route, id) {
        this.id = id;
        this.route = route
        this.fetch();
      },
      url: function() {
        var base = '';
        if(window.location.host === 'localhost') {
          base = 'http://192.168.33.10:3000';
        }
        return base + '/stop/' + this.route + '/' + this.id;
      }
    });

    return StopModel;
});
