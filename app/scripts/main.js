/*global require*/
'use strict';

require.config({
  shim: {
    underscore: {
      exports: '_'
    },
    backbone: {
      deps: [
        'underscore',
        'jquery'
      ],
      exports: 'Backbone'
    },
    handlebars: {
      exports: 'Handlebars'
    }
  },
  paths: {
    jquery: '../bower_components/jquery/jquery',
    backbone: '../bower_components/backbone/backbone',
    underscore: '../bower_components/underscore/underscore',
    handlebars: '../bower_components/handlebars/handlebars'
  }
});

require([
  'backbone',
  'routes/app',
  'views/Navigation'
], function (Backbone, AppRouter, NavigationView) {
  console.log("Starting App");

  var appRouter = new AppRouter,
      navigationView = new NavigationView({el: $('.menu .inner')});

  Backbone.history.start();
});
