/*global define*/

define([
    'jquery',
    'backbone',
    'views/RouteList',
    'views/RouteStopList',
    'views/Stop',
    'views/Nearest'
], function ($, Backbone, RouteListView, RouteStopListView, StopView, NearestView) {
    'use strict';

    var AppRouter = Backbone.Router.extend({
        routes: {
          '': 'index',
          'nearest': 'nearest',
          'routes': 'routeList',
          'route/:route/:direction': 'routeStopList',
          'stop/:route/:stopId': 'transitStop'
        },
        index: function() {
          var self = this;
          // checking if geolocation is possible
          if(Modernizr.geolocation) {
            navigator.geolocation.getCurrentPosition(function(pos){
              window.gpsLoc = pos;
              Backbone.history.navigate('nearest', {trigger: true, replace: true});
            }, function(err){
              Backbone.history.navigate('routes', {trigger: true, replace: true});
            });
          } else {
            Backbone.history.navigate('routes', {trigger: true, replace: true});
          }
        },
        nearest: function() {
          if(typeof window.gpsLoc === 'undefined') {
            Backbone.history.navigate('', {trigger: true, replace: true});
          } else {
            this.changeBackground();
            var nearest = new NearestView({
              el: $('.main .inner'),
              latitude: window.gpsLoc.coords.latitude,
              longitude: window.gpsLoc.coords.longitude
            });
          }
        },
        routeList: function() {
          this.changeBackground();
          var routeListView = new RouteListView({
            el: $('.main .inner')
          });
        },
        routeStopList: function(route, direction) {
          this.changeBackground(route);
          var routeStopList = new RouteStopListView({
            el: $('.main .inner'),
            route: route,
            direction: direction
          });
        },
        transitStop: function(route, stopId) {
          this.changeBackground(route);
          var stopView = new StopView({
            el: $('.main .inner'),
            id: stopId,
            route: route
          });
        },
        changeBackground: function(route) {
          $('.main').attr('class', function(i,c){
            return c.replace(/(^|\s)route-\S+/g, '');
          });

          if(typeof route !== 'undefined') {
            $('.main').addClass('route-' + route);
          }
        }
    });

    return AppRouter;
});
