# MUNI Arrivals Frontend
This is the frontend for MUNI Arrivals. Part of the Uber coding challenge.

Technical track: fullstack

This project has been split up into two projects in order to utilize tools 
to help manage development. On the front end I am utilizing yeoman with the
requre.js backbone generator. On the backend which I will detail more in 
that readme I am using a basic express.js setup. This project should only 
be checked out when doing development on the frontend. To deploy this project 
is added as a submodule to the backend. A dist is generated and symlinked as 
the public directory.

This is my first backbone project, and my first time using yeoman. I normally
work with ember.js and use Ember App Kit to help manage dev. Since there are 
currently lots of time constraints around this I have omitted a few items that 
I normally would do. I have skipped all testing because I am not familiar with 
mocha and chai. Also I was unsure of how to get require.js wired up correctly. 
Since there is also not that much business logic on the frontend most testing 
would be closer to acceptance tests rather than unit tests. This project is
also missing a CSS framework. Normally on any project I would LESS. Due to this,
all css transitions and animations are compatible with webkit with little 
concern for mozilla, opera, or IE. Given more time I would have added additional 
images to all the routes, and fine tuned the positioning and styling of the elements.

Design goals were to make this a mobile first application. So I attempted to make
a interface that would work well on a mobile device. UX Choices were arrived at 
organically. However, the position of the menu at the bottom was intentional. It 
based on research done by Luke Wroblewski on [Optimizing for Touch Across Devices](http://www.lukew.com/ff/entry.asp?1649).
Touch targets for some of the navigation also might be a bit too small, so that 
would have been an area I would have liked to focus on as well.

